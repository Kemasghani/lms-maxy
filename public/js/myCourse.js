// script.js

// Function to set the active tab based on localStorage
function setActiveTabFromLocalStorage() {
    var lastActiveTab = localStorage.getItem('lastActiveTab');
    if (lastActiveTab) {
        openTab(lastActiveTab);
    }
}

// Function to open a tab
function openTab(tabId) {
    // Hide all tab content
    var tabContents = document.querySelectorAll('.tab-content');
    tabContents.forEach(function (content) {
        content.classList.remove('active');
    });

    // Deactivate all tabs
    var tabs = document.querySelectorAll('.tab');
    tabs.forEach(function (tab) {
        tab.classList.remove('active');
    });

    // Show the clicked tab content and mark it as active
    document.getElementById(tabId).classList.add('active');
    event.currentTarget.classList.add('active');

    // Store the active tab in localStorage
    localStorage.setItem('lastActiveTab', tabId);
}

// Set the initial active tab
setActiveTabFromLocalStorage();
