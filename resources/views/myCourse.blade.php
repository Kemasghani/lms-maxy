<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/myCourse.css') }}">
</head>

<body>
    @extends('layouts.app')

    @section('content')
        <div class="container d-flex flex-nowrap gap-5">
            <div class="sidebar d-flex flex-column align-items-center gap-5">
                <div class="profileContainer d-flex flex-column text-center">
                    <div class="imageProfile">
                        <img src="{{ asset('images/profile.png') }}" alt="profile">
                    </div>
                    <span class="name">Agraditya Putra</span>
                    <span class="division">UI/UX Designer</span>
                </div>
                <div class="d-flex flex-column gap-2 menuContainer">
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center active">
                        <div>
                            <img src="{{ asset('images/dashboard.svg') }}" alt="dashboard">
                        </div>
                        <span>Dashboard</span>
                    </div>
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center">
                        <div>
                            <img src="{{ asset('images/myClasses.svg') }}" alt="myClasses">
                        </div>
                        <span>My Classes</span>
                    </div>
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center">
                        <div>
                            <img src="{{ asset('images/setting.svg') }}" alt="setting">
                        </div>
                        <span>Settings</span>
                    </div>
                </div>
            </div>
            <div class="w-100">
                <span class="tittleMyCourse">Enrolled Courses</span>
                <ul class="tabs mt-4" id="tabs">
                    <li class="tab active" onclick="openTab('tab1')">All Courses</li>
                    <li class="tab" onclick="openTab('tab2')">Active Courses</li>
                    <li class="tab" onclick="openTab('tab3')">Completed Courses</li>
                </ul>
                <div id="tab1" class="tab-content active">
                    <div class="itemMyCourseContainer d-flex flex-column gap-4 mt-5">
                        <div class="d-flex gap-5 itemMyCourse w-100">
                            <div class="thumbnailDetailCourse"><img src="{{ asset('images/thumbnailDetailCourse.png') }}" alt="thumbnailDetailCourse">
                            </div>
                            <div class="d-flex flex-column justify-content-between py-4 w-100">
                                <div class="d-flex gap-2">
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                </div>
                                <div class="tittleCardMyCourse">Rapid UI/UX Bootcamp</div>
                                <div class="d-flex gap-5 infoDetailMyCourse">
                                    <span>Total Lessons: <span class="subInfoDetailMyCourse">20</span></span>
                                    <span>Completed Lessons: <span class="subInfoDetailMyCourse">9/20</span></span>
                                </div>
                                <div class="d-flex flex-column gap-1">
                                    <span class="percentageComplete">70% complete</span>
                                    <div class="progressBarContainer">
                                        <div class="coloredProgressBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex gap-5 itemMyCourse w-100">
                            <div class="thumbnailDetailCourse"><img src="{{ asset('images/thumbnailDetailCourse.png') }}" alt="thumbnailDetailCourse">
                            </div>
                            <div class="d-flex flex-column justify-content-between py-4 w-100">
                                <div class="d-flex gap-2">
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                </div>
                                <div class="tittleCardMyCourse">Rapid UI/UX Bootcamp</div>
                                <div class="d-flex gap-5 infoDetailMyCourse">
                                    <span>Total Lessons: <span class="subInfoDetailMyCourse">20</span></span>
                                    <span>Completed Lessons: <span class="subInfoDetailMyCourse">9/20</span></span>
                                </div>
                                <div class="d-flex flex-column gap-1">
                                    <span class="percentageComplete">70% complete</span>
                                    <div class="progressBarContainer">
                                        <div class="coloredProgressBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex gap-5 itemMyCourse w-100">
                            <div class="thumbnailDetailCourse"><img src="{{ asset('images/thumbnailDetailCourse.png') }}" alt="thumbnailDetailCourse">
                            </div>
                            <div class="d-flex flex-column justify-content-between py-4 w-100">
                                <div class="d-flex gap-2">
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                </div>
                                <div class="tittleCardMyCourse">Rapid UI/UX Bootcamp</div>
                                <div class="d-flex gap-5 infoDetailMyCourse">
                                    <span>Total Lessons: <span class="subInfoDetailMyCourse">20</span></span>
                                    <span>Completed Lessons: <span class="subInfoDetailMyCourse">9/20</span></span>
                                </div>
                                <div class="d-flex flex-column gap-1">
                                    <span class="percentageComplete">70% complete</span>
                                    <div class="progressBarContainer">
                                        <div class="coloredProgressBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex gap-5 itemMyCourse w-100">
                            <div class="thumbnailDetailCourse"><img src="{{ asset('images/thumbnailDetailCourse.png') }}" alt="thumbnailDetailCourse">
                            </div>
                            <div class="d-flex flex-column justify-content-between py-4 w-100">
                                <div class="d-flex gap-2">
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                </div>
                                <div class="tittleCardMyCourse">Rapid UI/UX Bootcamp</div>
                                <div class="d-flex gap-5 infoDetailMyCourse">
                                    <span>Total Lessons: <span class="subInfoDetailMyCourse">20</span></span>
                                    <span>Completed Lessons: <span class="subInfoDetailMyCourse">9/20</span></span>
                                </div>
                                <div class="d-flex flex-column gap-1">
                                    <span class="percentageComplete">70% complete</span>
                                    <div class="progressBarContainer">
                                        <div class="coloredProgressBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex gap-5 itemMyCourse w-100">
                            <div class="thumbnailDetailCourse"><img src="{{ asset('images/thumbnailDetailCourse.png') }}" alt="thumbnailDetailCourse">
                            </div>
                            <div class="d-flex flex-column justify-content-between py-4 w-100">
                                <div class="d-flex gap-2">
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                    <div><img src="{{ asset('images/starFilled.svg') }}" alt="starFilled"></div>
                                </div>
                                <div class="tittleCardMyCourse">Rapid UI/UX Bootcamp</div>
                                <div class="d-flex gap-5 infoDetailMyCourse">
                                    <span>Total Lessons: <span class="subInfoDetailMyCourse">20</span></span>
                                    <span>Completed Lessons: <span class="subInfoDetailMyCourse">9/20</span></span>
                                </div>
                                <div class="d-flex flex-column gap-1">
                                    <span class="percentageComplete">70% complete</span>
                                    <div class="progressBarContainer">
                                        <div class="coloredProgressBar"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab2" class="tab-content">
                    <h2>Content for Tab 2</h2>
                    <p>This is the content for Tab 2.</p>
                </div>
                <div id="tab3" class="tab-content">
                    <h2>Content for Tab 3</h2>
                    <p>This is the content for Tab 3.</p>
                </div>
            </div>
        </div>
    @endsection
    <script src="{{ url('js/myCourse.js') }}"></script>
</body>

</html>
