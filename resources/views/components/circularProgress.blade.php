<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/circularProgress.css') }}">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="progress blue">
                <span class="progress-left">
                    <span class="progress-bar"></span>
                </span>
                <span class="progress-right">
                    <span class="progress-bar"></span>
                </span>
                <div class="progress-value d-flex flex-column text-center text-black gap-3">
                    <div id="valueProgress">90%</div>
                    <div id="progressText">of your progress</div>
                </div>
            </div>
        </div>
</body>

</html>
