<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
</head>

<body>
    @extends('layouts.app')

    @section('content')
        <div class="container d-flex flex-nowrap gap-5">
            <div class="sidebar d-flex flex-column align-items-center gap-5">
                <div class="profileContainer d-flex flex-column text-center">
                    <div class="imageProfile">
                        <img src="{{ asset('images/profile.png') }}" alt="profile">
                    </div>
                    <span class="name">Agraditya Putra</span>
                    <span class="division">UI/UX Designer</span>
                </div>
                <div class="d-flex flex-column gap-2 menuContainer">
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center active">
                        <div>
                            <img src="{{ asset('images/dashboard.svg') }}" alt="dashboard">
                        </div>
                        <span>Dashboard</span>
                    </div>
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center">
                        <div>
                            <img src="{{ asset('images/myClasses.svg') }}" alt="myClasses">
                        </div>
                        <span>My Classes</span>
                    </div>
                    <div class="d-flex gap-3 menuItem justify-content-start align-items-center">
                        <div>
                            <img src="{{ asset('images/setting.svg') }}" alt="setting">
                        </div>
                        <span>Settings</span>
                    </div>
                </div>
            </div>
            <div class="middle">
                <div class="d-flex flex-column gap-5">
                    <div class="statistic">
                        <div class="d-flex flex-column gap-2">
                            <span class="tittleStatistic">Statistics</span>
                            <span class="dateStatistic">Januart - June 2021</span>
                        </div>
                        <div class="d-flex justify-content-between">
                            <div class="d-flex flex-column gap-4 mt-5">
                                <div class="d-flex dalign-items-end gap-3">
                                    <div class="imageStatisticContainer imageAbsenceContainer">
                                        <img src="{{ asset('images/absence.svg') }}" alt="absence">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="nameItemStatistic text-nowrap">Absence</span>
                                        <span class="progressItemStatistic">90%</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end gap-3">
                                    <div class="imageStatisticContainer imageTasksContainer">
                                        <img src="{{ asset('images/task.svg') }}" alt="task">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="nameItemStatistic text-nowrap">Tasks & Exam</span>
                                        <span class="progressItemStatistic">60%</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-end gap-3">
                                    <div class="imageStatisticContainer imageQuizContainer">
                                        <img src="{{ asset('images/quiz.svg') }}" alt="quiz">
                                    </div>
                                    <div class="d-flex flex-column">
                                        <span class="nameItemStatistic text-nowrap">Quiz</span>
                                        <span class="progressItemStatistic">90%</span>
                                    </div>
                                </div>
                            </div>
                            <x-circularProgress />
                        </div>
                    </div>
                </div>
                <div class="doneTaskContainer d-flex flex-column gap-2">
                    <div class="doneTaskItem d-flex gap-2">
                        <div class="gap-2">
                            <div></div>
                            <div class="d-flex flex-column gap-2">
                                <div>Kamu sudah mempelajari <span class="boldText">Design UI/UX Fundamental</span> di
                                    kelas
                                    <span class="boldText">Rapid Bootcamp UI/UX</span>
                                </div>
                                <span class="dateDoneTaskItem">Yesterday</span>
                            </div>
                        </div>
                    </div>
                    <div class="doneTaskItem d-flex gap-2">
                        <div class="gap-2">
                            <div></div>
                            <div class="d-flex flex-column gap-2">
                                <div>Kamu sudah mempelajari <span class="boldText">Design UI/UX Fundamental</span> di
                                    kelas
                                    <span class="boldText">Rapid Bootcamp UI/UX</span>
                                </div>
                                <span class="dateDoneTaskItem">Yesterday</span>
                            </div>
                        </div>
                    </div>
                    <div class="doneTaskItem d-flex gap-2">
                        <div class="gap-2">
                            <div></div>
                            <div class="d-flex flex-column gap-2">
                                <div>Kamu sudah mempelajari <span class="boldText">Design UI/UX Fundamental</span> di
                                    kelas
                                    <span class="boldText">Rapid Bootcamp UI/UX</span>
                                </div>
                                <span class="dateDoneTaskItem">Yesterday</span>
                            </div>
                        </div>
                    </div>
                    <div class="doneTaskItem d-flex gap-2">
                        <div class="gap-2">
                            <div></div>
                            <div class="d-flex flex-column gap-2">
                                <div>Kamu sudah mempelajari <span class="boldText">Design UI/UX Fundamental</span> di
                                    kelas
                                    <span class="boldText">Rapid Bootcamp UI/UX</span>
                                </div>
                                <span class="dateDoneTaskItem">Yesterday</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="yourProgress">
            <div class="tittleRight mb-4">Your progress</div>
            <div class="progressBarContainer d-flex flex-column gap-4">
                <div class="progressItem d-flex flex-column gap-1">
                    <div class="textProgressBar d-flex justify-content-between">
                        <span class="class">Figma</span>
                        <span class="progressNumber">5/10</span>
                    </div>
                    <div class="progressBar">
                        <div class="coloredProgressBar"></div>
                    </div>
                </div>
                <div class="progressItem d-flex flex-column gap-1">
                    <div class="textProgressBar d-flex justify-content-between">
                        <span class="class">Figma</span>
                        <span class="progressNumber">5/10</span>
                    </div>
                    <div class="progressBar">
                        <div class="coloredProgressBar"></div>
                    </div>
                </div>
                <div class="progressItem d-flex flex-column gap-1">
                    <div class="textProgressBar d-flex justify-content-between">
                        <span class="class">Figma</span>
                        <span class="progressNumber">5/10</span>
                    </div>
                    <div class="progressBar">
                        <div class="coloredProgressBar"></div>
                    </div>
                </div>
                <div class="progressItem d-flex flex-column gap-1">
                    <div class="textProgressBar d-flex justify-content-between">
                        <span class="class">Figma</span>
                        <span class="progressNumber">5/10</span>
                    </div>
                    <div class="progressBar w-100">
                        <div class="coloredProgressBar"></div>
                    </div>
                </div>
                <div class="progressItem d-flex flex-column gap-1">
                    <div class="textProgressBar d-flex justify-content-between">
                        <span class="class">Figma</span>
                        <span class="progressNumber">5/10</span>
                    </div>
                    <div class="progressBar w-100">
                        <div class="coloredProgressBar"></div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
    <script src="{{ url('js/home.js') }}"></script>
</body>

</html>
